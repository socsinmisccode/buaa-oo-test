## dataMaker

Output test data to stdin.

## resChecker

Check `stdout.txt` with `stdin.txt`

## oneKeyTest

Usage:

```
./oneKeyTest.sh ${your-program}.jar ${cycles-for-test}
```

## Make

```
make
```

To build.

```
make clean
```

To clean.