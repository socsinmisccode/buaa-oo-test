all: src/dataMaker/main.c src/resChecker/main.c
	gcc -o dataMaker src/dataMaker/main.c
	gcc -o resChecker src/resChecker/main.c
	cp src/oneKeyTest.sh oneKeyTest.sh
	cp src/datainput_student_linux_x86_64 datainput
	touch stdin.txt
	touch stdout.txt
	chmod 777 oneKeyTest.sh
	chmod 777 datainput
clean: 
	rm dataMaker
	rm resChecker
	rm oneKeyTest.sh
	rm datainput
	rm stdin.txt
	rm stdout.txt
