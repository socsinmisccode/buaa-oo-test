#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_TIME 20.0
#define MAX_INSTR_CNT 70
#define MAX_PERSON_INSTR_CNT 30
#define MAX_PERSON_ID 100
#define MAX_FLOOR 11
#define ELEVATOR_CNT 6

typedef char *String;

int personUsed[MAX_INSTR_CNT + 1];
int personCnt = 0;
int elevatorReqCnt[ELEVATOR_CNT + 1];

int randomInt(int begin, int end);
double randomDouble(double begin, double end);

String makeInstr();

int main()
{
        srand((unsigned)time(NULL));
        memset(personUsed, 0, sizeof(personUsed));
        memset(elevatorReqCnt, 0, sizeof(elevatorReqCnt));

        double timeStick = 1.0;
        int instrCnt = randomInt(1, MAX_INSTR_CNT);

        for (int i = 0; i < instrCnt; i += 1)
        {
                timeStick = randomDouble(timeStick, (MAX_TIME - timeStick) / (instrCnt - i) + timeStick);
                printf("[%.1lf]%s\n", timeStick, makeInstr());
        }
        return 0;
}

int makePersonId();
int makeElevatorId();
int calcIntToStrSize(int src);

String makeInstr()
{
        int personId = makePersonId();
        int start = randomInt(1, MAX_FLOOR);
        int dest = randomInt(1, MAX_FLOOR);

        while (dest == start)
        {
                dest = randomInt(1, MAX_FLOOR);
        }

        int elevatorId = makeElevatorId();
        int strSize = calcIntToStrSize(personId) + calcIntToStrSize(start) + calcIntToStrSize(dest) +
                      calcIntToStrSize(elevatorId);
        String res = (String)malloc((strSize + 1) * sizeof(char));
        sprintf(res, "%d-FROM-%d-TO-%d-BY-%d", personId, start, dest, elevatorId);
        return res;
}

int calcIntToStrSize(int src)
{
        int size = 1;
        while (abs(src / 10) > 0)
        {
                src /= 10;
                size += 1;
        }
        return size;
}

int makeElevatorId()
{
        int id = randomInt(1, ELEVATOR_CNT);

        while (elevatorReqCnt[id] > 30)
        {
                id = randomInt(1, ELEVATOR_CNT);
        }
        elevatorReqCnt[id] += 1;
        return id;
}

int checkPersonIdUsed(int id);

int makePersonId()
{
        int id = randomInt(1, MAX_PERSON_ID);

        while (checkPersonIdUsed(id))
        {
                id = randomInt(1, MAX_PERSON_ID);
        }
        personUsed[personCnt] = id;
        personCnt += 1;
        return id;
}

int checkPersonIdUsed(int id)
{
        for (int i = 0; i < personCnt; i += 1)
        {
                if (personUsed[i] == id)
                {
                        return 1;
                }
        }
        return 0;
}

double randomDouble(double begin, double end)
{
        return begin + ((double)(rand() % RAND_MAX) / RAND_MAX) * (end - begin);
}

int randomInt(int begin, int end)
{
        return rand() % (end - begin + 1) + begin;
}
