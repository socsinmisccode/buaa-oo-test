#!/bin/bash
for i in $(seq 1 $2)
do
    echo Test Cycle $i
    ./dataMaker > stdin.txt
    ./datainput | java -jar $1 > stdout.txt
    ./resChecker
    if [ $? -ne 0 ]
    then
        echo Test Stop.
        break
    fi
done
